

import java.util.Scanner;

public class Grosshaendler {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Wie viele Meause wollen sie kaufen?");
		int kaufbetrag = scan.nextInt();
		double erg = 0;
		double maus = 14.99;
		erg = maus * kaufbetrag;
		if (kaufbetrag < 10) {
			erg = erg + 10;
			erg = erg * 1.19;
		} else {
			erg = erg * 1.19;
		}
		scan.close();

		System.out.println("-----------------------------");
		System.out.println("Bestellauftrag");
		System.out.println("Es wurden " + kaufbetrag + " Meause bestellt.");
		System.out.println("Der Preis einer Maus ist " + maus + "$");
		System.out.printf("Der Endpreis betraegt  %.2f $ \n", erg);
		System.out.println("-----------------------------");

	}

}
